# cryptsetup-vault

__*Note:*__ `cryptsetup-vault -setkey` will save the passphrase into the token's cubbyhole. If the token expires or gets revoked, that passphrase is lost. Make a backup!

## Installation

__*Note:*__ Currently only Arch Linux is tested/supported, if you are on another distro, your mileage may vary.

### Arch Linux

- [Install cryptsetup-vault via the AUR](https://aur.archlinux.org/packages/cryptsetup-vault/).
- Execute the Setup guide below to get a token and push your passphrase into vault
- Create a file in `/etc/cryptsetup-vault/credentials` (chmod 0600, chown root:root) with the following content:
```bash
VAULT_URL=<vault URL>
VAULT_TOKEN=<Token from previous step>
```
- Setup kernel parameters to set an IP or get one via DHCP on boot
  - Use `dmesg | awk '$0~/eno1: renamed from/ {print $NF}'` (replace eno1 with your NIC) to get the original NIC name for the parameter.
  - If your system is up a long time it might be, that the message is vanished. A safe bet would be to reboot and reexec that command after it to get the NIC
- Add the `netconf cryptsetupvault encrypt` hooks into /etc/mkinitcpio.conf (example below)
```bash
#...
HOOKS=(base udev autodetect modconf block keymap netconf cryptsetupvault encrypt resume filesystems keyboard fsck)
#...
```
- execute `mkinitcpio -p linux` as root
- Done! Reboot and enjoy!

## Setup the tokens and keys

__*Note:*__ This requires an existing Vault installation, with TLS and valid/trusted certificates.

- Create long-lived token (valid for 10 years - You may need to tweak your vault settings for this should you receive an error):
```bash
VAULT_TOKEN=<root/sudo token> VAULT_ADDR=<vault URL> vault token create --display-name "$(hostname -f) cryptsetup token" -renewable --orphan --no-default-policy --period 5256000m --ttl 5256000m --explicit-max-ttl=52560000m
```

- Create the file `/etc/cryptsetup-vault` (chmod 0600, chown root:root) with the following content:
```bash
VAULT_URL=<vault URL>
VAULT_TOKEN=<Token from previous step>
```

- Set key in vault for currently mounted & encrypted rootfs
```bash
sudo -E cryptsetup-vault -setkey
# enter password when prompted
```

- Check if the key in vault will actually unlock the drive
```bash
sudo -E cryptsetup-vault -test
```
