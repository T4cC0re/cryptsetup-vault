module gitlab.com/T4cC0re/cryptsetup-vault

go 1.14

require (
	github.com/hashicorp/vault/api v1.10.0
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/crypto v0.13.0
)
