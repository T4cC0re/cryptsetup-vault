package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh/terminal"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"time"
)

func init() {
	// We are handling sensitive key material. Lock it, so it's not swapped to disk in any case.
	err := syscall.Mlockall(syscall.MCL_CURRENT | syscall.MCL_FUTURE)
	if err != nil {
		handleFatalError(err)
	}
}

var (
	address          = flag.String("vault", getEnv("VAULT_ADDR"), "Vault address (e.g. https://my.vault.io)")
	token            = flag.String("token", "", "Vault token")
	cryptdevice      = flag.String("cryptdevice", getCryptdevice(), "cryptdevice to unlock")
	cryptmapper      = flag.String("cryptmapper", getCryptmapper(), "cryptmapper to mount -cryptdevice as")
	cryptoptions     = flag.String("cryptoptions", getCryptoptions(), "-cryptdevice options (allow-discards)")
	vaultReadTimeout = flag.Int64("timeout", getEnvInt64WithDefault("TIMEOUT", 60), "Amount of seconds to retry read from vault. (applies to 'read' only, 0 = never timeout)")
	rebootOnError    = flag.Bool("rebootonerror", getEnvBoolWithDefault("REBOOT", false), "Whether to reboot if the disk cannot be unlocked automatically")
	test             = flag.Bool("test", false, "Just test the passphrase retrieved from vault (but not actually luksOpen)")
	setKey           = flag.Bool("setkey", false, `(requires root) set this flag to set a key in vault for the specified UUID and exit.
If the command is executed on a terminal, you will be prompted.
Otherwise you can pipe in the passphrase *without newlines!* (e.g. 'echo -n')`)
	fullString = ""
	env        = map[string]string{}
	version    = "source"
)

func main() {
	flag.Parse()
	net.DefaultResolver.PreferGo = true

	cdUUID := getCryptDeviceUUID(*cryptdevice)
	if *token == "" {
		// Don't use the placeholder here, as that will output the token on -help
		*token = getEnv("VAULT_TOKEN")
	}
	log.Infof("cryptsetup-vault (%s) - secure and automated cryptsetup powered by vault", version)
	log.Infof("cryptdevice:\t\t\t%s", *cryptdevice)
	log.Infof("cryptdevice UUID:\t\t%s", cdUUID)
	log.Infof("cryptmapper:\t\t\t%s", *cryptmapper)
	log.Infof("cryptoptions:\t\t%s", *cryptoptions)
	log.Infof("Configured read timeout:\t%d", *vaultReadTimeout)
	log.Infof("VAULT_ADDR:\t\t\t%s", *address)
	log.Infof("VAULT_TOKEN:\t\t\t(redacted) length: %d", len(*token))

	if len(*address) == 0 || len(*token) == 0 {
		handleFatalErrorMsg("No address or token given. Cancelling...")
	}

	if len(cdUUID) == 0 || len(*cryptdevice) == 0 {
		handleFatalErrorMsg("Cryptdevice or mapper name missing (%s). Cancelling...", fullString)
	}

	vaultConfig := api.Config{
		Address: *address,
	}

	client, err := api.NewClient(&vaultConfig)
	if err != nil {
		log.Fatal(err)
	}
	client.SetToken(*token)

	if *setKey {
		var bytePassphrase []byte
		if terminal.IsTerminal(int(syscall.Stdin)) {
			log.Info("Reading passphrase from terminal...")
			fmt.Println("Enter Passphrase: ")
			bytePassphrase, err = terminal.ReadPassword(int(syscall.Stdin))
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Info("Reading passphrase from stdin...")
			bytePassphrase, _ = ioutil.ReadAll(os.Stdin)
		}
		passphrase := string(bytePassphrase)

		if err := validatePassphrase(cdUUID, passphrase); err != nil {
			log.Fatal(err)
		}

		data := map[string]interface{}{}
		data["passphrase"] = passphrase
		log.Infof("Setting key in vault...")
		_, err = client.Logical().Write("/cubbyhole/"+cdUUID, data)
		if err != nil {
			log.Fatal(err)
		}
		log.Info("Done")
		return
	}

	log.Infof("Reading key from vault...")

	secret, err := readFromVault(client, "/cubbyhole/"+cdUUID)
	if err != nil {
		handleFatalError(err)
	}
	if secret == nil && !*setKey {
		handleFatalErrorMsg("No key is set in vault for disk UUID %s", cdUUID)
	}

	var passphrase string

	for k, v := range secret.Data {
		if k != "passphrase" {
			continue
		}
		passphrase = v.(string)
	}

	if passphrase == "" {
		handleFatalErrorMsg("Passphrase for disk UUID %s is empty in vault!", cdUUID)
	}

	log.Infof("Received key from vault...")

	cryptArgs := compileCryptsetupArgs(*cryptoptions)

	if *test {
		if err := validatePassphrase(cdUUID, passphrase); err != nil {
			log.Fatal(err)
		}
	} else {
		err := unlockDisk(cdUUID, *cryptmapper, passphrase, cryptArgs)
		if err != nil {
			handleFatalError(err)
		}
	}

	log.Info("Done!")
	log.Info("Have a nice day!")

	os.Exit(0)
}

func handleFatalError(err error) {
	log.Error(err)
	if *rebootOnError {
		syscall.Sync()
		err = syscall.Reboot(syscall.LINUX_REBOOT_CMD_RESTART)
		log.Fatal(err) // This is only reached if the syscall fails
	} else {
		os.Exit(1)
	}
}

func handleFatalErrorMsg(format string, vars ...interface{}) {
	handleFatalError(errors.New(fmt.Sprintf(format, vars...)))
}

func loadEnvFromFile() (localenv map[string]string) {
	if len(env) > 0 {
		return
	}

	localenv = map[string]string{}

	data, _ := ioutil.ReadFile("/etc/cryptsetup-vault")
	lines := bytes.Split(data, []byte("\n"))
	for _, line := range lines {
		line = bytes.Trim(line, "\r\t ")
		splits := bytes.SplitN(line, []byte("="), 2)
		if len(splits) == 2 {
			localenv[string(splits[0])] = string(splits[1])
		}
	}
	env = localenv
	return
}

func getEnvInt64WithDefault(key string, _default int64) (value int64) {
	valueStr := getEnv(key)
	if valueStr == "" {
		value = _default
	} else {
		v, _ := strconv.Atoi(valueStr)
		value = int64(v)
	}
	return
}

func getEnvBoolWithDefault(key string, _default bool) (value bool) {
	valueStr := getEnv(key)
	if valueStr == "" {
		value = _default
	} else {
		switch valueStr {
		case "1", "true", "True", "t", "y", "Y":
			value = true
		default:
			value = false
		}
	}
	return
}

func getEnv(key string) (value string) {
	value = os.Getenv(key)
	if value != "" {
		return
	}
	loadEnvFromFile()

	return env[key]
}

func readFromVault(client *api.Client, path string) (secret *api.Secret, err error) {
	var timeout <-chan time.Time
	if *vaultReadTimeout == 0 {
		timeout = make(chan time.Time)
	} else {
		timeout = time.After(time.Duration(*vaultReadTimeout) * time.Second)
	}
	tick := time.Tick(500 * time.Millisecond)

	for {
		select {
		case <-tick:
			secret, err = client.Logical().Read(path)
			if err == nil {
				return
			}
			log.Error(err)
		case <-timeout:
			err = errors.New("reading from vault timed out")
			return
		}
	}
}

/**
will exit the program if the passphrase (and/or uuid) is invalid
*/
func validatePassphrase(uuid string, passphrase string) error {
	log.Infof("Validating passphrase against cryptsetup...")
	cmd := exec.Command("cryptsetup", "--test-passphrase", "--key-file", "-", "luksOpen", "/dev/disk/by-uuid/"+uuid)
	cmd.Stdin = strings.NewReader(passphrase)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

/**
will exit the program if the passphrase (and/or uuid) is invalid or the unlock failed somehow.
*/
func unlockDisk(uuid string, mappername string, passphrase string, opts []string) error {
	log.Infof("Unlocking disk...")
	args := []string{
		"--key-file", "-", "luksOpen", "/dev/disk/by-uuid/" + uuid, mappername,
	}
	args = append(opts, args...)
	cmd := exec.Command("cryptsetup", args...)
	cmd.Stdin = strings.NewReader(passphrase)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func compileCryptsetupArgs(opts string) (cryptArgs []string) {
	splits := strings.Split(opts, ",")
	for _, opt := range splits {
		switch opt {
		case "allow-discards":
			cryptArgs = append(cryptArgs, "--allow-discards")
		default:
			log.Warnf("cryptoption '%s' unknown, ignoring...", opt)
		}
	}
	return
}

func getCryptdevice() string {
	if fullString == "" {
		fullString = getCryptdeviceString()
	}

	return strings.SplitN(fullString, ":", 3)[0]
}
func getCryptmapper() string {
	if fullString == "" {
		fullString = getCryptdeviceString()
	}

	splits := strings.SplitN(fullString, ":", 3)
	if len(splits) > 1 {
		return splits[1]
	}
	return ""
}

func getCryptoptions() string {
	if fullString == "" {
		fullString = getCryptdeviceString()
	}

	splits := strings.SplitN(fullString, ":", 3)
	if len(splits) == 3 {
		return splits[2]
	}
	return ""
}

/**
guaranteed to always output a UUID or an empty string
*/
func getCryptDeviceUUID(dev string) string {

	if strings.HasPrefix(dev, "UUID=") {
		uuid := strings.Split(dev, "=")[1]
		filename := "/dev/disk/by-uuid/" + strings.ToLower(uuid)

		fi, _ := os.Lstat(filename)
		if fi != nil {
			return fi.Name()
		}
	}

	log.Fatal("currently only 'UUID='-style cryptdevice parameters are supported")

	return ""
}

func getCryptdeviceString() string {
	line, _ := ioutil.ReadFile("/proc/cmdline")
	splits := bytes.Split(line, []byte(" "))
	for _, split := range splits {
		if bytes.HasPrefix(split, []byte("cryptdevice=")) {
			return string(bytes.SplitN(split, []byte("="), 2)[1])
		}
	}

	return ""
}
