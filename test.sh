#!/usr/bin/env bash

set -e

go build -o cryptsetup-vault ./src
# No need to continue if this does not compile

function finish {
  rc=$?
  set +e
  export VAULT_TOKEN="test_token"
  kill -9 $VAULT_PID
  sudo cryptsetup close /dev/mapper/vault_test
  sudo losetup -d ${DEV}
  exit $rc
}

trap finish EXIT

vault server -dev -dev-root-token-id=test_token &>/dev/null &
VAULT_PID=$!

sleep 1

export VAULT_TOKEN="test_token"
export VAULT_ADDR="http://127.0.0.1:8200"

export VAULT_TOKEN=$(vault token create -display-name "$(hostname -f) cryptsetup token" -renewable --orphan -period 5256000m -ttl 5256000m -explicit-max-ttl=52560000m | jq -r .auth.client_token)

dd if=/dev/zero of=./.test_container bs=1M count=250
DEV=$(sudo losetup -f)
echo $DEV
sudo losetup ${DEV} ./.test_container

echo -n 'vault_test_password' | sudo cryptsetup -v --type luks --iter-time 250 --use-urandom luksFormat "${DEV}" -

for UUID in ls /dev/disk/by-uuid/*; do
  LD=$(readlink -f $UUID)
  if [ "$LD" = "$DEV" ]; then
    LDOK=1
    break
  fi
done

if ! [ "$LDOK" = "1" ]; then
  exit 128
fi

UUID=$(sudo blkid ${DEV} | awk -F '"' '$1~/UUID=/ {print $2}')

PARAMS="-cryptdevice UUID=${UUID} -cryptmapper vault_test -cryptoptions allow-discards -token test_token -vault http://127.0.0.1:8200"

echo -n 'vault_test_password' | sudo -E ./cryptsetup-vault ${PARAMS} -setkey
sudo -E ./cryptsetup-vault ${PARAMS}
test -b /dev/mapper/vault_test && echo "luksOpen successful"
